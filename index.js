const express = require("express");
const slugify = require("slugify");
var methodOverride = require('method-override')
const app = express();
app.use(methodOverride('_method'))
app.use(express.urlencoded({extended:true}))
app.use(express.static('public'));
const titulo = "";
const productos = [
    {id:1,nombre:"Coca Cola", precio: 11500, slug: "coca"}, 
    {id:2,nombre:"Mirinda", precio: 10500, slug: "mirinda"}, 
    {id:3,nombre:"Fanta", precio: 9500, slug: "fanta"}
];
app.set("view engine", "ejs");
app.set("views", "views");
//app.use(express.bodyParser())
//app.use(express.methodOverride())
app.use(express.json());
//peticion get
app.get("/productos", (req, res) => {
    res.render("productos", {titulo: titulo, productos: productos});
   
});

app.get("/productos_json", (req, res) => {
//res.send("Hola mundo")
//res.render("bienvenida", {titulo: titulo, productos: productos});
res.json(productos)
});
app.get("/crearproducto", (req, res) => {
    res.render("crearproducto");
    });

app.get ("/productos/:slug", (req, res) => {
    const p_slug = req.params.slug;
    const producto_seleccionado = productos.find(
        (producto) => (producto.slug === p_slug)
        );
    // console.log ("producto seleccionado: ", {producto_seleccionado}); 
    res.render("producto", {producto: producto_seleccionado})
});

app.put("/productos/:id", (req, res) => {
    const id = Number(req.params.id);
    const producto_editado = req.body;
    // Encuentra el índice del producto a editar en la lista
    const indiceProductoEditar = productos.findIndex(producto => producto.id === id);
    if (indiceProductoEditar !== -1) {
        // Reemplaza el producto en la lista con el producto editado
        productos[indiceProductoEditar].nombre = producto_editado.nombre;
        productos[indiceProductoEditar].precio = producto_editado.precio;
        productos[indiceProductoEditar].slug = slugify(producto_editado.nombre);

        // Puedes redirigir a una página de éxito o realizar alguna otra acción aquí
        res.redirect("/productos");
    } else {
        res.status(404).send("Producto no encontrado");
    }
});

app.delete("/productos/:id", (req, res) => {
    const id = req.params.id;
    const indiceProductoEliminar = productos.findIndex(producto => producto.id === Number(id));
    if (indiceProductoEliminar > -1) {
        // Elimina el producto de la lista
        productos.splice(indiceProductoEliminar, 1);
    } 
})

app.post("/productos", (req, res) =>{
    let id = Math.max(...productos.map((producto) => producto.id));
    if (id<=0) {
        id=0
    }
    const producto = {id: (id + 1) , nombre:req.body.nombre, 
        precio:Number(req.body.precio) , slug:slugify(req.body.nombre)} 
    
    productos.push(producto)
    res.redirect("/productos");
    
})

module.exports = app;