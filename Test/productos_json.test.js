const supertest = require('supertest');
const app = require('../index');
const { response } = require('..');

test ("Validar que la cabecera sea json", async() => {
    await supertest(app)
    .get('/productos_json')
    .expect(200)
    .then((response) => {
        expect(response.status).toBe(200);
        expect(response.headers["content-type"]).toEqual(
            "application/json; charset=utf-8"
        );
        
    });
});

test ("Verificar la dimension de mi array", async() => {
    await supertest(app)
    .get('/productos_json')
    .expect(200)
    .then((response) => {
        expect(response.status).toBe(200);
        expect(response.body).toHaveLength(3);
    });
});

test ("Verificar el valor del primer elemento del array", async() => {
    await supertest(app)
    .get('/productos_json')
    .expect(200)
    .then((response) => {
        expect(response.status).toBe(200);
        expect(response.body[0]).toHaveProperty("id", 1);
    });
});

test ("Verificar el valor del ultimo elemento del array", async() => {
    await supertest(app)
    .get('/productos_json')
    .expect(200)
    .then((response) => {
        var a = response.body.length;
        expect(response.status).toBe(200);
        expect(response.body[a-1]).toHaveProperty("id", 3);
    });
});